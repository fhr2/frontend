import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import * as RequestAction from 'store/actions/request.action'
import moment from "moment";
import {requestSelector} from "../../store/reducers/request.reducer";
import { Loader } from 'components/shared';

export const Home = () => {
    const dispatch = useDispatch()
    const formFields = {name: '', value: ''}
    const averageData = {daily: 0, hourly: 0, minutely: 0}
    const [allPostsData, setAllPostsData] = useState<any>([])
    const [getCall, setGetCall] = useState<boolean>(true)
    const [postsData, setPostsData] = useState<any>(formFields)
    const [responseError, setResponseError] = useState("");
    const [pageNumber, setPageNumber] = useState(0);
    const [postAverages, setPostAverages] = useState(averageData);
    const { isLoading: isRequestLoading } = useSelector(requestSelector)


    const getAllPosts = () => {
        dispatch(RequestAction.getPosts(pageNumber, async (responseData) => {
            setAllPostsData((oldArray: any) => allPostsData.concat(responseData.posts));
            setPostAverages({daily: responseData.daily_avg, hourly: responseData.hourly_avg, minutely: responseData.minutes_avg})
            setPageNumber(pageNumber+1)
        }));
    }
    // @ts-ignore
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(async () => {
        if (getCall){
            getAllPosts();
            setGetCall(false);
        }
    }, [getCall, getAllPosts]);
    const onSubmit =()=> {
        const body = {post: postsData}
        dispatch(RequestAction.createPost(body, {headers: {
                'content-type': 'application/json'
            }}, (response:any, err:any)=>{
            if(err){
                setResponseError(err.message);
            }else{
                setAllPostsData((oldArray: any) => [response.post].concat(allPostsData));
                setPostAverages({daily: response.daily_avg, hourly: response.hourly_avg, minutely: response.minutes_avg})
                setResponseError('');
                setPostsData(formFields);
            }
        }));
    };

    const onDataChangeEvent = (event: any) => {
        setPostsData((data: any) => ({
            ...data, [event.target.name]: (event.target.value)
        }))
    }
    const onLoadMore = () => {
        getAllPosts();
    }

    return(
        <div>
            <div className="form-wrapper">
                <div>
                    <p className="up-txt">Post Something</p>
                    {responseError &&
                        <div className="error_help_container">
                            <div className="error_help_txt">
                                <span className="error_txt2">{responseError}</span>
                            </div>
                        </div>
                    }
                    <div className="form-input-wrapper">
                        <div className='row'>
                            <div className='col-6'>
                                <div className="key-search-box">
                                    <span className="search-box">
                                        {Object.keys(formFields).map((key:any, index:number) => {
                                            return (
                                                <input
                                                    name={key}
                                                    type='text'
                                                    placeholder={`type a ${key}`}
                                                    value={postsData[`${key}`]}
                                                    title={`Add ${key}`}
                                                    onChange={event => onDataChangeEvent(event)}
                                                    required={true}
                                                    key={index+key}
                                                />
                                            )})
                                        }
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="submit-btn-div">
                            <button type='button' className="app-btn"
                                    onClick={() => {
                                        onSubmit()
                                    }}>SUBMIT
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container mt-5 mb-5 form-wrapper">
                <div className="row">
                    <div className="top-head">
                        <div className="top-head-left">
                            <h3 className="right-menu montserrat">Latest Posts</h3>
                        </div>
                    </div>
                    <div className="top-head">
                        <div className="average-section">
                            <h3 className="right-menu montserrat">{`Daily Avg: ${postAverages.daily}`}</h3>
                        </div>
                    </div>
                    <div className="top-head">
                        <div className="average-section">
                            <h3 className="right-menu montserrat">{`Hourly Avg: ${postAverages.hourly}`}</h3>
                        </div>
                    </div>
                    <div className="top-head">
                        <div className="average-section">
                            <h3 className="right-menu montserrat">{`Minute Avg: ${postAverages.minutely}`}</h3>
                        </div>
                    </div>
            </div>
            <div className="container mt-5 mb-5 form-wrapper">
                </div>
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <ul className="timeline">
                            {allPostsData &&
                                allPostsData.map((post:any, i:any) => {
                                    return(
                                        <li key={post.id + post.created_at + i}>
                                            <a href="#">{post.name}</a>
                                            <a href="#" className="float-right">{moment(post.created_at).format('DD MMM, YYYY - hh:mm:ss')}</a>
                                            <p>{post.value}</p>
                                        </li>)
                                })
                            }
                        </ul>
                        <div className="submit-btn-div">
                            <button type='button' className="app-btn"
                                    onClick={() => {
                                        onLoadMore()
                                    }}>Load More...
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {isRequestLoading && <Loader />}
        </div>
    )
}
