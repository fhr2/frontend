import React from 'react';

export const Header = () => {
    return (
        <div>
            <div className="head-section">
                <div className="container">
                    <div className="row">
                        <div className="top-head">
                            <div className="top-head-center">
                                <h3 className="right-menu montserrat head-title">F-Hr Assignment</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
