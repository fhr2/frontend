import { paths } from '../apis'
import axios from "axios";
export const REQUEST_LOADING = 'REQUEST_LOADING' as const;
export const REQUEST_LOADED = 'REQUEST_LOADED' as const;
export const REQUEST_LOADING_FAILED = 'REQUEST_LOADING_FAILED' as const;
const BASE_URL = process.env.REACT_APP_BASE_API_URL
const requestLoading = () => ({
    type: REQUEST_LOADING
})

const requestLoaded = (response: any) => ({
    type: REQUEST_LOADED,
    payload: response
})

const requestLoadingFailed = (response: any, err: string) => ({
    type: REQUEST_LOADING_FAILED,
    payload: response,
    err
})

export type Actions =
    | ReturnType<typeof requestLoading>
    | ReturnType<typeof requestLoaded>
    | ReturnType<typeof requestLoadingFailed>

export const getPosts = (pageNumber:any, callback?: (res:any, err:any) => void): any => (dispatch:any) => {
    dispatch(requestLoading());
    axios.get((BASE_URL + paths.getPosts), { params: { page: pageNumber }} )
        .then(res => {
            dispatch(requestLoaded(res));
            callback&&callback(res.data, null);
        })
        .catch(err => {
            if(!err){ err = {message: "No response from server."}}
            dispatch(requestLoadingFailed(null, err.message))
            callback&&callback(null, err);
        })
}
export const createPost = (formData: any, config:any, callback?: (res:any, err:any) => void): any => (dispatch:any) => {
    dispatch(requestLoading());
    axios.post((BASE_URL + paths.createPost), formData, config)
        .then(res => {
            dispatch(requestLoaded(res));
            callback&&callback(res.data, null);
        })
        .catch(err => {
            if(!err){ err = {message: "No response from server."}}
            dispatch(requestLoadingFailed(formData, err.message))
            callback&&callback(null, err);
        })
}
