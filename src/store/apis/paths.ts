const paths = {
    createPost: '/api/posts',
    getPosts: '/api/posts',
    getPostsAverages: '/api/posts/posts_average',
}

export default paths;
